#ifndef _GLOBAL_H
#define _GLOBAL_H

#include <pthread.h>

typedef struct temp_hum{
  float temp, hum;
}temp_hum;

typedef struct sensores{
  int psen1, psen2,
      osen1, osen2,
      osen3, osen4,
      osen5, osen6;
  int alarm;
}sensores;

extern temp_hum th;

extern int row, col;

extern pthread_t t_get_th, t_server;

extern sensores sns;

extern int controlados[6];

#endif
