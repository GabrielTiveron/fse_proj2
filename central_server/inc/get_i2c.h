#ifndef _GET_I2C_H
#define _GET_I2C_H

#include <pthread.h>
#include <ncurses.h>
#include <unistd.h>

#include "cliente_tcp.h"

void init_th();
void * get_temp_hum();

#endif
