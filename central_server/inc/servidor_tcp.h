#ifndef _SERVIDOR_TCP_H
#define _SERVIDOR_TCP_H


#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "global.h"
#include "menu.h"

int check_event(int*,int);
void handle_sns_log(int*,int,char*);
void set_sns(sensores s);
void trata_cliente_tcp(int);
void* socket_server();


#endif
