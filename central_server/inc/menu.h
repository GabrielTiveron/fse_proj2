#ifndef MENU_H
#define MENU_H

#include<ncurses.h>
#include<unistd.h>
#include<string.h>
#include<pthread.h>

#include "global.h"
#include "cliente_tcp.h"

int entrada_padrao;
int row, col;

void inicia_menu();
void draw_box(int,int,int,int);
void draw_menu();
void print_menu(int);
void* control_menu();
void print_sensores();
void insert_infos(int,int,int);
void insert_infos_sensores(int,int,int);
void end_prog();

#endif
