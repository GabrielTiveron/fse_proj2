#include "../inc/write_file.h"

void write_headers(){
  FILE *fp;
  fp = fopen (FILE_NAME,"w");
  if (fp!=NULL){
    fprintf(fp,"Evento;Horario\n");
    fclose (fp);
  }
}

void write_event(char*event){
  FILE *fp;
  fp = fopen (FILE_NAME,"a");
  time_t rawtime;
  struct tm* timeinfo;

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  if (fp!=NULL){
    fprintf(fp,"%s;%s", event, asctime(timeinfo));
    fclose (fp);
  }
}
