#include "../inc/servidor_tcp.h"

int check_event(int*s, int n){
  if(*s == n){
    return 0;
  }
  else{
    *s = n;
    if(*s && sns.alarm){
      system("aplay ../../utils/alarm.mp3");
    }
    return 1;
  }
}

void handle_sns_log(int *s, int n, char*sensor){
  char str[20];
  if(check_event(s, n)){
    sprintf(str,"%s %s", sensor, (*s == 1) ? "Acionado" : "Desativado");
    write_event(str);
  }
}

void set_sns(sensores s){
  handle_sns_log(&sns.osen1, s.osen1, "OSEN1");
  handle_sns_log(&sns.osen2, s.osen2, "OSEN2");
  handle_sns_log(&sns.osen3, s.osen3, "OSEN3");
  handle_sns_log(&sns.osen4, s.osen4, "OSEN4");
  handle_sns_log(&sns.osen5, s.osen5, "OSEN5");
  handle_sns_log(&sns.osen6, s.osen6, "OSEN6");
  handle_sns_log(&sns.osen6, s.osen6, "OSEN6");
  handle_sns_log(&sns.psen1, s.psen1, "PSEN1");
  handle_sns_log(&sns.psen2, s.psen2, "PSEN2");
}

void trata_cliente_tcp(int socketCliente) {
	sensores buffer;
	int tamanhoRecebido = sizeof(sensores);

	if((tamanhoRecebido = recv(socketCliente, &buffer, tamanhoRecebido, 0)) < 0)
		printf("Erro no recv()\n");

 	set_sns(buffer);
 	print_sensores();
}

void* socket_server() {
	int servidorSocket;
	int socketCliente;
	struct sockaddr_in servidorAddr_;
	struct sockaddr_in clienteAddr;
	unsigned short servidorPorta;
	unsigned int clienteLength;

	servidorPorta = PORT;

	// Abrir Socket
	if((servidorSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("falha no socker do Servidor\n");

	// Montar a estrutura sockaddr_in
	memset(&servidorAddr_, 0, sizeof(servidorAddr_)); // Zerando a estrutura de dados
	servidorAddr_.sin_family = AF_INET;
	servidorAddr_.sin_addr.s_addr = htonl(INADDR_ANY);
	servidorAddr_.sin_port = htons(servidorPorta);

	// Bind
	if(bind(servidorSocket, (struct sockaddr *) &servidorAddr_, sizeof(servidorAddr_)) < 0)
		printf("Falha no Bind\n");

	// Listen
	if(listen(servidorSocket, 10) < 0)
		printf("Falha no Listen\n");

	while(1) {
		clienteLength = sizeof(clienteAddr);
		if((socketCliente = accept(servidorSocket,
			                      (struct sockaddr *) &clienteAddr,
			                      &clienteLength)) < 0)
		printf("Falha no Accept\n");

		trata_cliente_tcp(socketCliente);

	}
	close(servidorSocket);

}
