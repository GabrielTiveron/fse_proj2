#include "../inc/get_i2c.h"

// void init_th(){
//   pthread_t t;
//   pthread_create(&t, NULL, get_temp_hum, NULL);
// }

void * get_temp_hum(){
 int x_temp = row / 4 +1;
 int x_hum = row / 4 + 3;
 int y_th = col / 4 + 2;

  while(1){
    send_temp_hum(-1);
    attron(COLOR_PAIR(4));
    mvprintw(x_temp, y_th + 13, "%.2f C", th.temp);
    mvprintw(x_hum, y_th + 9, "%.2f%%", th.hum);
    attroff(COLOR_PAIR(4));
    refresh();
    sleep(1);
  }
}
