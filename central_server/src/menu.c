#include"../inc/menu.h"

char *opcoes[] = {
                    "LAMP 1",
                    "LAMP 2",
                    "LAMP 3",
                    "LAMP 4",
                    "AIRC 1",
                    "AIRC 2",
                    "ALARM ",
                    "EXIT"
                  };
int highlight = 1;

void end_prog(){
  // End distributed server
  send_message(-100);
  close_socket();
  endwin();
  exit(0);
}

void inicia_menu(){
	initscr();
	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_BLACK, COLOR_WHITE);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	cbreak();
	keypad(stdscr,TRUE);
	getmaxyx(stdscr, row, col);

  // Get sensors initial state
  send_message(-1);
  // Get Lamps/Aircs initial state
  send_message(-2);

	print_sensores();

}

void draw_box(int height, int width, int x, int y){
  for(int i = x; i < width; i++){
    for(int j = y; j < height; j++){
      if((i == x || i == width - 1) && (j > y && j < height - 1)){
        mvaddch(i,j,ACS_HLINE);
        continue;
        }
      else if(i == width - 1 && j == y){
        mvaddch(i,j,ACS_LLCORNER);
        continue;
        }
      else if(i == width -1 && j == height - 1){
        mvaddch(i,j,ACS_LRCORNER);
        continue;
        }
      else if(i == x && j == y){ // Esq sup
        mvaddch(i,j,ACS_ULCORNER);
        continue;
        }
      else if(i == x && j == height -1){
        mvaddch(i,j,ACS_URCORNER);
        continue;
        }
      else if((i > x && i < width-1) && (j == y || j == height - 1)){
        mvaddch(i,j,ACS_VLINE);
        continue;
        }
        }
    }
}

void insert_text(int row, int col){
  // Segundo quadrante
  mvprintw(row / 4 + 1,col / 4 + 2, "Temperatura: ");
  mvprintw(row / 4 + 3,col / 4 + 2, "Umidade: ");
  // Quarto quadrante
  mvprintw(row / 3 + 2,col / 2 + 1, "PSEN1: ");
  mvprintw(row / 3 + 4,col / 2 + 1, "PSEN2: ");
  mvprintw(row / 3 + 6,col / 2 + 1, "OSEN1: ");
  mvprintw(row / 3 + 8,col / 2 + 1, "OSEN2: ");
  mvprintw(row / 3 + 10,col / 2 + 1, "OSEN3: ");
  mvprintw(row / 3 + 12,col / 2 + 1, "OSEN4: ");
  mvprintw(row / 3 + 14,col / 2 + 1, "OSEN5: ");
  mvprintw(row / 3 + 16,col / 2 + 1, "OSEN6: ");
  // Primeiro quadrante
  mvprintw(row / 4 + 1, col / 2 + 1, "Projeto 2 - FSE");
  mvprintw(row / 4 + 3, col / 2 + 1, "Aluno - Gabriel Tiveron");
}

void print_sensores(){
  int x, y;
  x = row / 3 + 2;
  y = col / 2 + 1 + 7;
  insert_infos_sensores(x, y, sns.psen1);
  insert_infos_sensores(x+2, y, sns.psen2);
  insert_infos_sensores(x+4, y, sns.osen1);
  insert_infos_sensores(x+6, y, sns.osen2);
  insert_infos_sensores(x+8, y, sns.osen3);
  insert_infos_sensores(x+10, y, sns.osen4);
  insert_infos_sensores(x+12, y, sns.osen5);
  insert_infos_sensores(x+14, y, sns.osen6);
  refresh();
}

void print_menu(int highlight){
  int x, y, i;
  x = row / 3 + 1;
  y = col / 4 + 2;

  for(i = 0; i < 8; i++){
    if(highlight == i + 1){
      attron(COLOR_PAIR(3));
      mvprintw(x, y, "%s", opcoes[i]);
      attroff(COLOR_PAIR(3));
    }
    else{
      mvprintw(x, y, "%s", opcoes[i]);
    }
    if(i < 6)
      insert_infos(x, y+17, controlados[i]+1);
    if(i == 6)
      insert_infos_sensores(x, y+17, sns.alarm);

    x+=2;
  }
  refresh();
}

void *control_menu(){
  usleep(500000);
  int cmd;
  while(1){
    cmd = getch();
    switch(cmd){
      case KEY_UP:
        if(highlight == 1)
          highlight = 8;
        else
          highlight--;
      break;
      case KEY_DOWN:
        if(highlight == 8)
          highlight = 1;
        else
          highlight++;
      break;
      case 10:
        if(highlight == 7)
          sns.alarm = !sns.alarm;
        else if(highlight == 8)
          end_prog();
        else
          send_message(highlight);
        break;
    }
    print_menu(highlight);
  }
}

void draw_menu(){
  draw_box(col - col/4,row - row/4, row/4, col/4);
  draw_box(col - col/4,row/3+1, row/4,col/4);
  draw_box(col/2,row - row/4, row/4,col/4);
  insert_text(row, col);
  print_menu(1);
  pthread_t t;
  pthread_create(&t, NULL, control_menu, NULL);
  refresh();
}

void insert_infos(int x, int y, int flag){
  attron(COLOR_PAIR(flag));
  if(flag == 1)
	  mvprintw(x, y, "DESLIGADO");
  else
	  mvprintw(x, y, "LIGADO   ");

  attroff(COLOR_PAIR(flag));
}

void insert_infos_sensores(int x, int y, int flag){
  attron(COLOR_PAIR(flag+1));
  if(flag == 1)
	  mvprintw(x, y, "DETECTADO");
  else
	  mvprintw(x, y, "STANDBY  ");

  attroff(COLOR_PAIR(flag+1));
}
