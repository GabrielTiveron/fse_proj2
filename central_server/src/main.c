#include<unistd.h>

#include "../inc/menu.h"
#include "../inc/global.h"
#include "../inc/get_i2c.h"
#include "../inc/servidor_tcp.h"

int main(){
	init_client();
	inicia_menu();
	write_headers();
	pthread_create(&t_get_th, NULL, get_temp_hum, NULL);
	pthread_create(&t_server, NULL, socket_server, NULL);
	draw_menu();
	pause();

	return 0;
}
