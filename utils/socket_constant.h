#ifndef _CONSTANT_H
#define _CONSTANT_H

#define PORT 10128

#define LAMP1_TURNOFF 0
#define LAMP1_TURNON  1
#define LAMP2_TURNOFF 2
#define LAMP2_TURNON  3
#define LAMP3_TURNOFF 4
#define LAMP3_TURNON  5
#define LAMP4_TURNOFF 6
#define LAMP4_TURNON  7
#define AIRC1_TURNOFF 8
#define AIRC1_TURNON  9
#define AIRC2_TURNOFF 10
#define AIRC2_TURNON  11

#define PSEN1_SB 12
#define PSEN1_DE 13
#define PSEN2_SB 14
#define PSEN2_DE 15
#define OSEN1_SB 16
#define OSEN1_DE 17
#define OSEN2_SB 18
#define OSEN2_DE 19
#define OSEN3_SB 20
#define OSEN3_DE 21
#define OSEN4_SB 22
#define OSEN4_DE 23
#define OSEN5_SB 24
#define OSEN5_DE 25
#define OSEN6_SB 26
#define OSEN6_DE 27


#endif
