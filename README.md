## Projeto 2 - Fundamentos de Sistemas Operacionais

### Execução

Para a execução do projeto é necessário que primeiramente se inicie o servidor distribuído, para isso basta entrar na pasta ```distributed_server/``` e rodar o comando ```make```, por fim execute o comando ```./distributed```. Tendo o servidor distribuído sido executado pode-se então iniciar o servidor central, utilizando o mesmo comando para gerar o executável na pasta ```central_server``` e executar o programa com ```./central```.

Para uma melhor experiência é recomendado que execute o servidor central em tela cheia.

### Utilização

![imagem_menu](./img/menu.png)

O programa consiste em um simples menu contendo a interface de usuário para o controle do servidor distribuído, assim, o usuário pode selecionar as opções de lâmpadas e ar-condicionados para que sejam ligados ou desligados. Também é possível fazer o acompanhamento dos sensores de presença e abertura, os quais o programa coleta seus valores iniciais ao se iniciado, sendo todos atualizados após a cada interrupção. Por fim, é possível também acompanhar a temperatura e umidade, que são atualizados a cada 1 segundo.

### Referência

[Enunciado do Projeto](https://gitlab.com/fse_fga/projetos_2020_2/projeto-2-2020.2)

[Menu da aplicação](https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/)
