#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

#include "../inc/global.h"
#include "../inc/gpio.h"
#include "../inc/i2c.h"
#include "../inc/servidor_tcp.h"
#include "../inc/interrupcao.h"

struct timeval last_changed;

void init_sen_state(){
  sns.psen1 = digitalRead(PSEN1_ADDR);
  sns.psen2 = digitalRead(PSEN2_ADDR);
  sns.osen1 = digitalRead(OSEN1_ADDR);
  sns.osen2 = digitalRead(OSEN2_ADDR);
  sns.osen3 = digitalRead(OSEN3_ADDR);
  sns.osen4 = digitalRead(OSEN4_ADDR);
  sns.osen5 = digitalRead(OSEN5_ADDR);
  sns.osen6 = digitalRead(OSEN6_ADDR);
}

void init_sys(){
  if(wiringPiSetup() == -1){
    printf("Cannot setup wiringPi.\n");
    exit(1);
  }

  init_gpio();

  gettimeofday(&last_changed, NULL);
  lc_psen1 = last_changed;
  lc_psen2 = last_changed;
  lc_osen1 = last_changed;
  lc_osen2 = last_changed;
  lc_osen3 = last_changed;
  lc_osen4 = last_changed;
  lc_osen5 = last_changed;
  lc_osen6 = last_changed;

  init_sen_state();
  set_interrupt();

}

int main(){
  init_sys();

  socket_server();

  return 0;
}
