#include"../inc/i2c.h"
#include"../inc/bme280.h"

void get_bme(){
  int8_t rslt = BME280_OK;
  struct bme280_dev dev;
  struct identifier id;
  
  id.dev_addr = BME280_I2C_ADDR_PRIM;
  dev.intf = BME280_I2C_INTF;
  dev.read = user_i2c_read;
  dev.write = user_i2c_write;
  dev.delay_us = user_delay_us;

  dev.intf_ptr = &id;
  char i2c_[] = "/dev/i2c-1";

  if((id.fd = open(i2c_, O_RDWR)) < 0){
    fprintf(stderr, "Failed to open the i2c bus %X\n", BME280_ADD);
    return get_bme();
  }



  if(ioctl(id.fd, I2C_SLAVE, id.dev_addr) < 0){
    fprintf(stderr, "Failed to acquire bus access and/or talk to slave.\n");
  }


  rslt = bme280_init(&dev);
  if(rslt != BME280_OK){
    fprintf(stderr, "Failed to stream sensor data (code %+d).\n", rslt);
  }

  rslt = set_sensor(&dev);

  close(id.fd);

}

int8_t user_i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr){
  struct identifier id = *((struct identifier*) intf_ptr);

  write(id.fd, &reg_addr, 1);
  read(id.fd, data, len);

  return 0;
}

void user_delay_us(uint32_t period, void *intf_ptr){
  usleep(period);
}

void print_sensor_data(struct bme280_data *comp_data){
    float temp, hum;

#ifdef BME280_FLOAT_ENABLE
    temp = comp_data->temperature;
    hum  = comp_data->humidity; 
#else
#ifdef BME280_64BIT_ENABLE
    temp = 0.01f * comp_data->temperature;
    hum  = 0.0001f * comp_data->humidity;
#else
    temp = 0.01f * comp_data->temperature;
    hum  = 1.0f / 1024.0f * comp_data->humidity;
#endif
#endif
    th.temp = temp;
    th.hum = hum;
//    printf("%0.2lf deg C, %0.2lf hPa, %0.2lf%%\n", temp, press, hum);
}

int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr){
    uint8_t *buf;
    struct identifier id;

    id = *((struct identifier *)intf_ptr);

    buf = malloc(len + 1);
    buf[0] = reg_addr;
    memcpy(buf + 1, data, len);
    if (write(id.fd, buf, len + 1) < (uint16_t)len)
    {
        return BME280_E_COMM_FAIL;
    }

    free(buf);

    return BME280_OK;
}

int8_t set_sensor(struct bme280_dev *dev){
int8_t rslt;
	uint8_t settings_sel;
	struct bme280_data comp_data;

	/* Recommended mode of operation: Indoor navigation */
	dev->settings.osr_h = BME280_OVERSAMPLING_1X;
	dev->settings.osr_p = BME280_OVERSAMPLING_16X;
	dev->settings.osr_t = BME280_OVERSAMPLING_2X;
	dev->settings.filter = BME280_FILTER_COEFF_16;
	dev->settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

	settings_sel = BME280_OSR_PRESS_SEL;
	settings_sel |= BME280_OSR_TEMP_SEL;
	settings_sel |= BME280_OSR_HUM_SEL;
	settings_sel |= BME280_STANDBY_SEL;
	settings_sel |= BME280_FILTER_SEL;
	rslt = bme280_set_sensor_settings(settings_sel, dev);
	rslt = bme280_set_sensor_mode(BME280_NORMAL_MODE, dev);

	//printf("Temperature, Pressure, Humidity\r\n");
//	while (1) {
		/* Delay while the sensor completes a measurement */
  	dev->delay_us(100000, dev->intf_ptr);
		rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, dev);
		print_sensor_data(&comp_data);
  //  break;
	//}

	return rslt;
}
