#include "../inc/interrupcao.h"

void verifica_sensor(int* s, int addr){
  *s = digitalRead(addr);
  send_sns(0);
  if(*s){
    printf("Rising - %d\n", addr);
  }
  else{
    printf("Falling - %d\n", addr);
  }
}

unsigned long get_diff(struct timeval*lc){
  struct timeval now;
  unsigned long diff;

  gettimeofday(&now, NULL);

  diff = (now.tv_sec *1000000 + now.tv_usec) - (lc->tv_sec * 1000000 + lc->tv_usec);

  *lc = now;

  return diff;
}

void trata_sinal_psen1(){
  if(get_diff(&lc_psen1) > IGNORE_TIME){
    verifica_sensor(&sns.psen1, PSEN1_ADDR);
  }
}

void trata_sinal_psen2(){
  if(get_diff(&lc_psen2) > IGNORE_TIME){
    verifica_sensor(&sns.psen2, PSEN2_ADDR);
  }
}

void trata_sinal_osen1(){
  if(get_diff(&lc_osen1) > IGNORE_TIME){
    verifica_sensor(&sns.osen1, OSEN1_ADDR);
  }
}

void trata_sinal_osen2(){
  if(get_diff(&lc_osen2) > IGNORE_TIME){
    verifica_sensor(&sns.osen2, OSEN2_ADDR);
  }
}

void trata_sinal_osen3(){
  if(get_diff(&lc_osen3) > IGNORE_TIME){
    verifica_sensor(&sns.osen3, OSEN3_ADDR);
  }
}

void trata_sinal_osen4(){
  if(get_diff(&lc_osen4) > IGNORE_TIME){
    verifica_sensor(&sns.osen4, OSEN4_ADDR);
  }
}

void trata_sinal_osen5(){
  if(get_diff(&lc_osen5) > IGNORE_TIME){
    verifica_sensor(&sns.osen5, OSEN5_ADDR);
  }
}

void trata_sinal_osen6(){
  if(get_diff(&lc_osen6) > IGNORE_TIME){
    verifica_sensor(&sns.osen6, OSEN6_ADDR);
  }
}

void set_interrupt(){
  wiringPiISR(PSEN1_ADDR, INT_EDGE_BOTH, trata_sinal_psen1);
  wiringPiISR(PSEN2_ADDR, INT_EDGE_BOTH, trata_sinal_psen2);
  wiringPiISR(OSEN1_ADDR, INT_EDGE_BOTH, trata_sinal_osen1);
  wiringPiISR(OSEN2_ADDR, INT_EDGE_BOTH, trata_sinal_osen2);
  wiringPiISR(OSEN3_ADDR, INT_EDGE_BOTH, trata_sinal_osen3);
  wiringPiISR(OSEN4_ADDR, INT_EDGE_BOTH, trata_sinal_osen4);
  wiringPiISR(OSEN5_ADDR, INT_EDGE_BOTH, trata_sinal_osen5);
  wiringPiISR(OSEN6_ADDR, INT_EDGE_BOTH, trata_sinal_osen6);
}
