#include "../inc/servidor_tcp.h"

int comunication_protocol[6] = {0,1,2,3,23,24};

int trata_cliente_tcp(int socketCliente) {
	int buffer;
	int tamanhoRecebido = sizeof(int);

	if((tamanhoRecebido = recv(socketCliente, &buffer, tamanhoRecebido, 0)) < 0)
		printf("Erro no recv()\n");


	while (tamanhoRecebido > 0) {
	 if(buffer == -1){
		 // Get temperature and humidity
	   get_bme();
	   tamanhoRecebido = sizeof(th);
	   if(send(socketCliente, &th, tamanhoRecebido, 0) != tamanhoRecebido)
	     printf("Erro no envio - send()\n");

	 }
	 else if(buffer < -90){
		 // Exit program
		 return 0;
	 }
	 else if(buffer == -2){
		 // Get initial State of sensors
		 tamanhoRecebido = sizeof(sensores);
		 if(send(socketCliente, &sns, tamanhoRecebido, 0) != tamanhoRecebido)
			 printf("Erro no envio - send()\n");
	 }
	 else if(buffer == -3){
		 // Get initial state of lamps and air
		 int *lamp_air_state = malloc(6*sizeof(int));
		 get_lamps_aic(lamp_air_state);
		 tamanhoRecebido = 6 * sizeof(int);
		 if(send(socketCliente, lamp_air_state, tamanhoRecebido, 0) != tamanhoRecebido)
		   printf("Erro no envio - send()\n");
		 free(lamp_air_state);
	 }
	 else{
		  // Send protocol
      int send_int = 2 * buffer + change_state(comunication_protocol[buffer]);
      if(send(socketCliente, &send_int, tamanhoRecebido, 0) != tamanhoRecebido)
        printf("Erro no envio - send()\n");
   }
		if((tamanhoRecebido = recv(socketCliente, &buffer, sizeof(int), 0)) < 0)
			printf("Erro no recv()\n");
	}

	return 1;
}

void  socket_server() {
	int servidorSocket;
	int socketCliente;
	struct sockaddr_in servidorAddr;
	struct sockaddr_in clienteAddr;
	unsigned short servidorPorta;
	unsigned int clienteLength;

	servidorPorta = PORT;

	// Abrir Socket
	if((servidorSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("falha no socker do Servidor\n");

	// Montar a estrutura sockaddr_in
	memset(&servidorAddr, 0, sizeof(servidorAddr)); // Zerando a estrutura de dados
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servidorAddr.sin_port = htons(servidorPorta);

	// Bind
	if(bind(servidorSocket, (struct sockaddr *) &servidorAddr, sizeof(servidorAddr)) < 0)
		printf("Falha no Bind\n");

	// Listen
	if(listen(servidorSocket, 10) < 0)
		printf("Falha no Listen\n");
  int res = 1;
	while(res) {
		clienteLength = sizeof(clienteAddr);
		if((socketCliente = accept(servidorSocket,
			                      (struct sockaddr *) &clienteAddr,
			                      &clienteLength)) < 0)
			printf("Falha no Accept\n");

		res = trata_cliente_tcp(socketCliente);

	}
	close(servidorSocket);

	exit(0);

}
