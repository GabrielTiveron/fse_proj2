#include "../inc/cliente_tcp.h"

int check_sns(){
  return sns.osen1 || sns.osen2 ||
         sns.osen3 || sns.osen4 ||
         sns.osen5 || sns.osen6 ||
         sns.psen1 || sns.psen2;
}

void send_sns(int alarm){
  if(init_client()){
    if(send(clienteSocket, &sns, sizeof(sensores), 0) != sizeof(sensores))
      printf("Erro no envio: numero de bytes enviados diferente do esperado\n");

      close(clienteSocket);
  }
  printf("Não foi possível conectar ao servidor central\n");
}

int init_client() {
	// Criar Socket
	if((clienteSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("Erro no socket()\n");

	// Construir struct sockaddr_in
	memset(&servidorAddr, 0, sizeof(servidorAddr)); // Zerando a estrutura de dados
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
	servidorAddr.sin_port = htons(PORT);

	printf("port - %d\nIP - %s\n", PORT,SERVER_IP);

	// Connect
	if(connect(clienteSocket, (struct sockaddr *) &servidorAddr,
							sizeof(servidorAddr)) < 0){
		printf("Erro no connect()\n");
    return 0;
  }

  return 1;

}
