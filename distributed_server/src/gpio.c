#include"../inc/gpio.h"

void init_gpio(){
  pinMode(LAMP1_ADDR, OUTPUT);
  pinMode(LAMP2_ADDR, OUTPUT);
  pinMode(LAMP3_ADDR, OUTPUT);
  pinMode(LAMP4_ADDR, OUTPUT);
  pinMode(AIRC1_ADDR, OUTPUT);
  pinMode(AIRC2_ADDR, OUTPUT);

  pinMode(PSEN1_ADDR, INPUT);
  pinMode(PSEN2_ADDR, INPUT);
  pinMode(OSEN1_ADDR, INPUT);
  pinMode(OSEN2_ADDR, INPUT);
  pinMode(OSEN3_ADDR, INPUT);
  pinMode(OSEN4_ADDR, INPUT);
  pinMode(OSEN5_ADDR, INPUT);
  pinMode(OSEN6_ADDR, INPUT);
}

void turn_on(int addr){
  digitalWrite(addr, HIGH);
}

void turn_off(int addr){
  digitalWrite(addr, LOW);
}

int change_state(int addr){
  int r = 0;
  if(digitalRead(addr)){
    turn_off(addr);
  }
  else{
    turn_on(addr);
    r = 1;
  }
  return r;
}

void get_lamps_aic(int*t){
  t[0] = digitalRead(LAMP1_ADDR);
  t[1] = digitalRead(LAMP2_ADDR);
  t[2] = digitalRead(LAMP3_ADDR);
  t[3] = digitalRead(LAMP4_ADDR);
  t[4] = digitalRead(AIRC1_ADDR);
  t[5] = digitalRead(AIRC2_ADDR);
}

void end_gpio(){
  digitalWrite(LAMP1_ADDR, LOW);
  digitalWrite(LAMP2_ADDR, LOW);
  digitalWrite(LAMP3_ADDR, LOW);
  digitalWrite(LAMP4_ADDR, LOW);
  digitalWrite(AIRC1_ADDR, LOW);
  digitalWrite(AIRC2_ADDR, LOW);
}
