#ifndef GLOBAL_H
#define GLOBAL_H

#include <sys/time.h>

// OUT ADDRESSES
#define LAMP1_ADDR 0
#define LAMP2_ADDR 1
#define LAMP3_ADDR 2
#define LAMP4_ADDR 3
#define AIRC1_ADDR 23
#define AIRC2_ADDR 24

// IN ADDRESSES
#define PSEN1_ADDR 6 
#define PSEN2_ADDR 25
#define OSEN1_ADDR 21
#define OSEN2_ADDR 22
#define OSEN3_ADDR 26
#define OSEN4_ADDR 27
#define OSEN5_ADDR 28
#define OSEN6_ADDR 29

typedef struct temp_hum{
  float temp, hum;
}temp_hum;

typedef struct sensores{
  int psen1, psen2,
      osen1, osen2,
      osen3, osen4,
      osen5, osen6;
  int alarm;
}sensores;

extern temp_hum th;

extern sensores sns; 
           
extern struct timeval lc_psen1, lc_psen2,
                      lc_osen1, lc_osen2,
                      lc_osen3, lc_osen4,
                      lc_osen5, lc_osen6;


#endif
