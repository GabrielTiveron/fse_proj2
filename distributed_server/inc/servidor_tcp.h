#ifndef _SERVIDOR_TCP_H
#define _SERVIDOR_TCP_H


#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../../utils/socket_constant.h"
#include "../inc/gpio.h"
#include "../inc/i2c.h"


int trata_cliente_tcp(int);
void socket_server();


#endif
