#ifndef _CLIENTE_TCP_H
#define _CLIENTE_TCP_H

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../inc/global.h"

#define PORT 10128
#define SERVER_IP "192.168.0.53"



int clienteSocket;
struct sockaddr_in servidorAddr;

int check_sns();
void send_sns(int);
int init_client();

#endif
