#ifndef GPIO_H
#define GPIO_H

#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "./global.h"

#define DELAY_GPIO 10000

void init_gpio();
void get_lamps_aic(int*);
int change_state(int);
void turn_on(int);
void turn_off(int);
void end_gpio();


#endif
